﻿using UnityEngine;
using System.Collections;

public class GaivotaMovement : MonoBehaviour {

    public float speed;
    public float x;
    // Use this for initialization
    void Start()
    {

        speed = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        x = Time.deltaTime * speed;
        gameObject.transform.Translate(Vector3.left * x);
    }
}
