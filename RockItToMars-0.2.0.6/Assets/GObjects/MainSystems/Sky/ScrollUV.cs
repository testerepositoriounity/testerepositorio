﻿using UnityEngine;
using System.Collections;

public class ScrollUV : MonoBehaviour {
    void Update () {


        MeshRenderer mr = GetComponent<MeshRenderer>();

        Material mat = mr.material;

        Vector2 offset = mat.mainTextureOffset;

        offset.y += Time.deltaTime/10;

        mat.mainTextureOffset = offset;
        if (offset.y >= 0.2)
            offset.y = 0;
	}
}
