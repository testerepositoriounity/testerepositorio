﻿using UnityEngine;
using System.Collections;

public class BackgroundSystem : MonoBehaviour {
    public GameObject sky;
    public GameObject sky2;
    public GameObject sky3;
    public GameObject sky4;
    public GameObject sky5;
    public GameObject sky6;
    public GameObject sky7;
    public GameObject sky8;
    public GameObject sky9;
    public GameObject sky10;
    public GameObject sky11;
    public GameObject sky12;
    public GameObject sky13;
    public GameObject sky14;

    public GameObject skyGradient;
    public GameObject skyGradient2;
    public GameObject skyGradient3;
    public GameObject skyGradient4;
    public GameObject skyGradient5;
    public GameObject skyGradient6;
    public GameObject skyGradient7;
    public GameObject skyGradient8;
    public GameObject skyGradient9;
    public GameObject skyGradient10;
    public GameObject skyGradient11;
    public GameObject skyGradient12;
    public GameObject skyGradient13;

    private GameObject rocket;
    private Vector2 rocketPosition;
    private Vector2 oldRocketPosition;
    private GameObject oldSky;
    private GameObject currentSky;
    private GameObject nextSky;
    private GameObject newSky;
    private Vector2 positionModifier;
    private Vector2 cloudPosModifier;
    public bool gameEnded;
    public int counter;
    
    void Start()
    {
        
        currentSky = GameObject.Find("Sky");
        nextSky = GameObject.Find("Sky2");
        rocket = GameObject.Find("Rocket");
        positionModifier.y = 0f;
        positionModifier.x = 0f;
        cloudPosModifier.y = 0f;
        cloudPosModifier.x = 0f;
        counter = 0;
        gameEnded = false;
    }
	void Update () {
        rocketPosition = rocket.transform.position;
        if (rocketPosition.y >= currentSky.transform.position.y -0.9 && !gameEnded)
        {
            DestroyObject(oldSky);
            positionModifier.y = rocketPosition.y + 30f;
            cloudPosModifier.y = rocketPosition.y + 16f;
            
            gameObject.GetComponent<FuelSpawn>().deleteFuel();
            gameObject.GetComponent<Barriers>().deleteBarrier();
            gameObject.GetComponent<Clouds>().deleteCloud();

            
            if (counter < 3)
            {
                sky.transform.position = positionModifier;
                newSky = Instantiate(sky);
            }

            else if (counter >= 3 && counter <=3)
            {
                skyGradient.transform.position = positionModifier;
                newSky = Instantiate(skyGradient);
            }

            else if (counter >= 3 && counter <= 10)
            {
                sky2.transform.position = positionModifier;
                newSky = Instantiate(sky2);
            }
            else if (counter >= 10 && counter <= 11)
            {
                skyGradient2.transform.position = positionModifier;
                newSky = Instantiate(skyGradient2);
            }
            else if (counter >= 11 && counter <= 16)
            {
                sky3.transform.position = positionModifier;
                newSky = Instantiate(sky3);
            }
            else if (counter >= 16 && counter <= 17)
            {
                skyGradient3.transform.position = positionModifier;
                newSky = Instantiate(skyGradient3);
            }
            else if (counter >= 17 && counter <= 26)
            {
                sky4.transform.position = positionModifier;
                newSky = Instantiate(sky4);
            }
            else if (counter >= 26 && counter <= 27)
            {
                skyGradient4.transform.position = positionModifier;
                newSky = Instantiate(skyGradient4);
            }
            else if (counter >= 27 && counter <= 36)
            {
                sky5.transform.position = positionModifier;
                newSky = Instantiate(sky5);
            }
            else if (counter >= 36 && counter <= 37)
            {
                skyGradient5.transform.position = positionModifier;
                newSky = Instantiate(skyGradient5);
            }
            else if (counter >= 37 && counter <= 46)
            {
                sky6.transform.position = positionModifier;
                newSky = Instantiate(sky6);
            }
            else if (counter >= 46 && counter <= 47)
            {
                skyGradient6.transform.position = positionModifier;
                newSky = Instantiate(skyGradient6);
            }
            else if (counter >= 47 && counter <= 56)
            {
                sky7.transform.position = positionModifier;
                newSky = Instantiate(sky7);
            }
            else if (counter >= 56 && counter <= 57)
            {
                skyGradient7.transform.position = positionModifier;
                newSky = Instantiate(skyGradient7);
            }
            else if (counter >= 57 && counter <= 68)
            {
                sky8.transform.position = positionModifier;
                newSky = Instantiate(sky8);
            }
            else if (counter >= 68 && counter <= 69)
            {
                skyGradient8.transform.position = positionModifier;
                newSky = Instantiate(skyGradient8);
            }
            else if (counter >= 69 && counter <= 80)
            {
                sky9.transform.position = positionModifier;
                newSky = Instantiate(sky9);
            }
            else if (counter >= 80 && counter <= 81)
            {
                skyGradient9.transform.position = positionModifier;
                newSky = Instantiate(skyGradient9);
            }
            else if (counter >= 81 && counter <= 92)
            {
                sky10.transform.position = positionModifier;
                newSky = Instantiate(sky10);
            }
            else if (counter >= 92 && counter <= 93)
            {
                skyGradient10.transform.position = positionModifier;
                newSky = Instantiate(skyGradient10);
            }
            else if (counter >= 93 && counter <= 108)
            {
                sky11.transform.position = positionModifier;
                newSky = Instantiate(sky11);
            }
            else if (counter >= 108 && counter <= 109)
            {
                skyGradient11.transform.position = positionModifier;
                newSky = Instantiate(skyGradient11);
            }
            else if (counter >= 109 && counter <= 124)
            {
                sky12.transform.position = positionModifier;
                newSky = Instantiate(sky12);
            }
            else if (counter >= 124 && counter <= 125)
            {
                skyGradient12.transform.position = positionModifier;
                newSky = Instantiate(skyGradient12);
            }
            else if (counter >= 125 && counter <= 140)
            {
                sky13.transform.position = positionModifier;
                newSky = Instantiate(sky13);
            }
            else if (counter >= 140 && counter <= 141)
            {
                skyGradient13.transform.position = positionModifier;
                newSky = Instantiate(skyGradient13);
            }
            else if (counter > 141)
            {
                sky14.transform.position = positionModifier;
                newSky = Instantiate(sky14);
            }
            
            int rand = (int)Random.Range(0f, 2f);

            if (rand == 0)
            {
                gameObject.GetComponent<FuelSpawn>().spawnFuel(positionModifier);
                

            }
            else if (rand == 1)
            {
                gameObject.GetComponent<Barriers>().spawnBarrier(positionModifier);
            }
            gameObject.GetComponent<Clouds>().spawnCloud(cloudPosModifier);



            oldSky = currentSky;
            currentSky = nextSky;
            nextSky = newSky;
            counter++;
            
        }

    }
    
}
