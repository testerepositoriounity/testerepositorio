﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Debugador : MonoBehaviour
{
    private Rocket rocket;
    private Slider slider;
    private FuelTank fTank;
    private Points points;
    // Use this for initialization
    void Start()
    {
        points = GameObject.Find("System").GetComponent<Points>();
        rocket = GameObject.Find("Rocket").GetComponent<Rocket>();
        slider = GameObject.Find("StartSlider").GetComponent<Slider>();
        fTank = GameObject.Find("Rocket").GetComponent<FuelTank>();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Text>().text = ("Points: " + points.points);
    }
}
