﻿using UnityEngine;
using System.Collections;

public class CameraFunc : MonoBehaviour {

    private Quaternion rotation;
    private GameObject rocket;
    private Vector3 position;
    private bool gameEnded;
    private Vector3 yModder;
    // Use this for initialization
    void Start () {
        rotation.x = 0;
        rotation.y = 0;
        rotation.z = 0;
        rotation.w = 0;
        rocket = GameObject.Find("Rocket");

        gameEnded = false;

    }

    // Update is called once per frame
    void Update () {
        position = rocket.transform.position;
        position.z = -20;
        position.x = 0f;
        yModder.y = 5.0f;
        if (!gameEnded)
            gameObject.transform.position = position + yModder;
        else 
            endMovement();
        

        gameObject.transform.rotation = rotation;
	}
    public void endMovement()
    {
        gameEnded = true;
        gameObject.transform.position = gameObject.transform.position;
        
    }

  


}
