﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FuelTank : MonoBehaviour {

    // Use this for initialization
    public float maxFuel;
    public float minFuel;
    public float currentFuel;

    void Start () {
        minFuel = 0;
        maxFuel = 100f;
        currentFuel = maxFuel;

        
    }
	
	// Update is called once per frame
	void Update () {

        if (currentFuel >= maxFuel)
        {
            currentFuel = maxFuel;

        }
        if (currentFuel <= minFuel)
        {
            currentFuel = minFuel;

        }
    }
    public void subractFuel(float fuel)
    {
        if (currentFuel - fuel <= minFuel)
        {
            currentFuel = minFuel;

        }
        else
        {
            currentFuel -= fuel;

        }        
    }

    public void addFuel(float fuel)
    {
        if (currentFuel + fuel >= maxFuel)
        {
            currentFuel = maxFuel;
        }
        else
        {
            currentFuel += fuel;
        }
    }
}