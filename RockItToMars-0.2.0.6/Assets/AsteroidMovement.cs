﻿using UnityEngine;
using System.Collections;

public class AsteroidMovement : MonoBehaviour {

    public float speed;
    public float y;
    // Use this for initialization
    void Start()
    {

        speed = 4f;
    }

    // Update is called once per frame
    void Update()
    {
        y = Time.deltaTime * speed;
        gameObject.transform.Translate(Vector3.down * y);
    }
}
