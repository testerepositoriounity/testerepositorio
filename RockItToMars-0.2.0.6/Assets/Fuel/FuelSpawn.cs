﻿using UnityEngine;
using System.Collections;

public class FuelSpawn : MonoBehaviour
{
    public GameObject fuel;
    public GameObject fuel2;
    public GameObject fuel3;
    public GameObject fuel4;
    public GameObject fuel5;
    public GameObject fuel6;
    private GameObject instantiatedFuel;
    private GameObject currentInstantiatedFuel;
    private GameObject oldInstantiatedFuel;
    public float fFormations;
    private float rand;
    public int randomized;
    private float posRand;
    void Start()
    {
        if (fFormations == 0)
            fFormations = 1f;
    }

    void Update()
    {

    }
    public void spawnFuel(Vector2 position)
    {
        oldInstantiatedFuel = currentInstantiatedFuel;
        currentInstantiatedFuel = instantiatedFuel;
        rand = Random.Range(0f, fFormations);
        randomized = (int)rand;
        posRand = Random.Range(-2.5f, 2.5f);
        if (randomized == 0)
        {
            if (posRand <= -1.9f)
                position.x = posRand + 1.8f;
            else
                position.x = posRand - 1.8f;

            fuel.transform.position = position;
            instantiatedFuel = Instantiate(fuel);
        }
        else if (randomized == 1)
        {
            
            fuel2.transform.position = position;
            instantiatedFuel = Instantiate(fuel2);
        }
        else if (randomized == 2)
        {
            if (posRand <= -2.0f)
                position.x = posRand + 0.5f;
            else
                position.x = posRand - 0.5f;
            fuel3.transform.position = position;
            instantiatedFuel = Instantiate(fuel3);
        }
        else if (randomized == 3)
        {
            if (posRand <= -1.8f)
                position.x = posRand + 1.8f;
            else
                position.x = posRand - 1.8f;
            fuel4.transform.position = position;
            instantiatedFuel = Instantiate(fuel4);
        }
        else if (randomized == 4)
        {
            //vazio
            fuel5.transform.position = position;
            instantiatedFuel = Instantiate(fuel5);
        }
        else if (randomized == 5)
        {
            //vazio
            fuel5.transform.position = position;
            instantiatedFuel = Instantiate(fuel5);
        }
        else if (randomized == 6)
        {
            fuel6.transform.position = position;
            instantiatedFuel = Instantiate(fuel6);
        }
        
    }
    public void deleteFuel()
    {
            Destroy(oldInstantiatedFuel);
    }
}
