﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class SetResolutionMenu : MonoBehaviour {

    public Dropdown res;
    public Dropdown windowMode;
    public int dropValueRes;
    public int dropValueWindow;


   



    void Start()

    {
       
        getResValue();
        //getWindowValue();
    
        

    }

    void Update()
    {
        
    }



    

    public void getResValue()
    {

        if (Screen.width == 1280)
        {

            res.value = 0;
        }

        else if (Screen.width == 1366)
        {
            res.value = 1;
        }

        else if (Screen.width == 1920)
        {
            res.value = 2;
        }

    }
          

    

  /*  public void getWindowValue()

    {
        if(!Screen.fullScreen)
        {

            windowMode.value = 1;
        }

        else
        {
           
            windowMode.value = 0;

        }
    }*/

   
    public void changeRes()
     
    {

        if (res.value == 0)
        {
            Screen.SetResolution(1280, 720, true, 60);
        }

        else if (res.value == 1)
        {
            Screen.SetResolution(1366, 768, true, 60);

        }
        else if (res.value == 2)
        {
            Screen.SetResolution(1920, 1080, true, 60);
        }

    }


    public void changeWindow()
    {
        if (windowMode.GetComponent<Dropdown>().value == 0)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = false;
        }
    }


}
