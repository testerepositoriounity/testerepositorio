﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SelectLevel : MonoBehaviour {

    public GameObject UI;

	// Use this for initialization
	void Start () {
        UI = GameObject.Find("UI");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void loadLevel1()
    {
        SceneManager.LoadScene(1);
        
        UI.GetComponent<ShowPanels>().HideMenu();


    }
    public void loadLevel2()
    {
        SceneManager.LoadScene(2);
        
        UI.GetComponent<ShowPanels>().HideMenu();


    }
    public void loadLevel3()
    {
        SceneManager.LoadScene(3);
        
        UI.GetComponent<ShowPanels>().HideMenu();


    }
}
