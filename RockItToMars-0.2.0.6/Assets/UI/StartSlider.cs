﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartSlider : MonoBehaviour {
    private Slider startSlider;
    private bool min, max;
    public float speed;
	// Use this for initialization
	void Start () {
        startSlider = gameObject.GetComponent<Slider>();
        min = true;
        max = false;
        speed = 120.0f;
	}
	
	// Update is called once per frame
	void Update () {
	if (startSlider.value < startSlider.maxValue && min)
        {
            startSlider.value+= speed * Time.deltaTime;
            if (startSlider.value >= startSlider.maxValue)
            {
                max = true;
                min = false;
            }
        }
    if(startSlider.value >= startSlider.minValue && max)
        {
            startSlider.value-= speed * Time.deltaTime;
            if (startSlider.value <= startSlider.minValue)
            {
                min = true;
                max = false;
            }
        }
	}
}
