﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ShowPanels : MonoBehaviour {

    public GameObject menu;
    public GameObject options;				
    public GameObject credits;
    public GameObject howtoPlay;
    public GameObject pauseMenu;
    public GameObject gameOver;

    





    public void Start()
    {

    }
	//Call this function to activate and display the Options panel during the main menu
	
    public void ShowMenu()
    {
        menu.SetActive(true);
    }
    public void HideMenu()
    {
        menu.SetActive(false);
    }
    public void ShowOptions()
    {
        options.SetActive(true);
    }
    public void HideOptions()
    {
        options.SetActive(false);
    }
    public void ShowCredits()
    {
        credits.SetActive(true);
    }
    public void HideCredits()
    {
        credits.SetActive(false);
    }
    public void ShowHowToPlay()
    {
        howtoPlay.SetActive(true);
    }
    public void HideHowToPlay()
    {
        howtoPlay.SetActive(false);
    }

    public void ShowPauseMenu()
    {
        pauseMenu.SetActive(true);
    }
    public void HidePauseMenu()
    {
        pauseMenu.SetActive(false);
    }

    public void ShowGameOver()
    {
        gameOver.SetActive(true);
    }
    public void HideGameOver()
    {
        gameOver.SetActive(false);
    }



}
