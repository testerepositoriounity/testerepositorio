﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FuelSlider : MonoBehaviour {

    public Slider fuelSlider;
    public float fuel;

	// Use this for initialization
	void Start () {
        fuelSlider = gameObject.GetComponent<Slider>();
        
        

    }
	
	// Update is called once per frame
	void Update () {
        fuel = GameObject.Find("Rocket").GetComponent<FuelTank>().currentFuel;
        fuelSlider.value = fuel;
	
	}
}
