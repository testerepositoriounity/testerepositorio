﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour
{
    public GameObject gameHud;

    public bool isPaused;
    public bool isInMenu;

    // Use this for initialization
    void Start()
    {
        isPaused = false;
        isInMenu = true;
        

    }

    // Update is called once per frame
    void Update()
    {
        pause();


    }

    public void pause()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && isPaused == false && isInMenu == false)
        {
            isPaused = true;
            gameHud = GameObject.Find("GameHud");
            gameObject.GetComponent<ShowPanels>().ShowPauseMenu();
            
            gameHud.SetActive(false);
            Time.timeScale = 0;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isPaused == true)
        {
            isPaused = false;
            gameObject.GetComponent<ShowPanels>().HidePauseMenu();
            
            gameHud.SetActive(true);
            Time.timeScale = 1;
        }
    }

    public void pauseBack()
    {
        isPaused = false;
        gameObject.GetComponent<ShowPanels>().HidePauseMenu();

        gameHud.SetActive(true);
        Time.timeScale = 1;
    }
    public void menuExit()
    {
        isInMenu = false;
    }
    public void pauseChange()
    {
        if (isPaused == true)
        {
            isPaused = false;
        }
        else
        {
            isPaused = true;
        }
    }
}