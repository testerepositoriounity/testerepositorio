﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {
    private Pause pause;
    private PlayMusic playMusic;
    private PlaySfx playSfx;
    private int sceneId;
    public ShowPanels showPanels;
    // Use this for initialization
    void Start () {
        pause = GameObject.Find("UI").GetComponent<Pause>();
        playMusic = GameObject.Find("Audio").GetComponent<PlayMusic>();
        playSfx = GameObject.Find("Audio").GetComponent<PlaySfx>();
        showPanels = GameObject.Find("UI").GetComponent<ShowPanels>();

    }
	
	// Update is called once per frame
	void Update () {
        sceneId = SceneManager.GetActiveScene().buildIndex;
    }
     void Awake()
    {
        //playMusic = GetComponent<PlayMusic>();
    }

    public void restart()
    {

        SceneManager.LoadScene(sceneId);
        showPanels.HideGameOver();
        //playMusic.musicStop();
        playSfx.stopWhileFly();
        Time.timeScale = 1;
        pause.isPaused = false;
    }
}
