﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

    public ShowPanels showPanel;

	// Use this for initialization
	void Start () {
        showPanel = GameObject.Find("UI").GetComponent<ShowPanels>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void startGame()
    {
        SceneManager.LoadScene(1);
        showPanel.HideMenu();
    }
}
