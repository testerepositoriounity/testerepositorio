﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class SetResolutionPause : MonoBehaviour
{

    public Dropdown resPause;
    public Dropdown windowModePause;
    public int dropValueRes;
    public int dropValueWindow;






    void Start()

    {
        
        getResValuePause();
        getWindowValuePause();

    }

    void Update()
    {

    }





    public void getResValuePause()
    {

        if (Screen.width == 1280)
        {

            resPause.value = 0;
        }

        else if (Screen.width == 1366)
        {
            resPause.value = 1;
        }

        else if (Screen.width == 1920)
        {
            resPause.value = 2;
        }

    }




    public void getWindowValuePause()

    {
        if (!Screen.fullScreen)
        {

            windowModePause.value = 1;
        }

        else
        {

            windowModePause.value = 0;

        }
    }


    public void changeRes()

    {

        if (resPause.value == 0)
        {
            Screen.SetResolution(1280, 720, true, 60);
        }

        else if (resPause.value == 1)
        {
            Screen.SetResolution(1366, 768, true, 60);

        }
        else if (resPause.value == 2)
        {
            Screen.SetResolution(1920, 1080, true, 60);
        }

    }


    public void changeWindow()
    {
        if (windowModePause.value == 0)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = false;
        }
    }


}