﻿using UnityEngine;
using System.Collections;

public class PanelManager : MonoBehaviour {

    public GameObject gameOverPanel;
    public GameObject pausePanel;


	// Use this for initialization
	void Start () {
	
	}

    public void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
    }

    public void HideGameOverPanel()
    {
        gameOverPanel.SetActive(false);
    }
}
