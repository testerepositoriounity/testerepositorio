﻿using UnityEngine;
using System.Collections;

public class Clouds : MonoBehaviour
{
    public GameObject cloud;
    public GameObject cloud2;
    public GameObject cloud3;
    public GameObject cloud4;
    public GameObject cloud5;
    public GameObject cloud6;
    public GameObject cloud7;
    public GameObject star;
    public GameObject star2;
    public GameObject star3;
    public GameObject star4;

    private GameObject instantiatedCloud;
    private GameObject currentInstantiatedCloud;
    private GameObject oldInstantiatedCloud;
    
    public float csFormations;
    private float rand;
    public int randomized;
    private float posRand;
    void Start()
    {
        if (csFormations == 0)
            csFormations = 10f;
    }

    void Update()
    {

    }
    public void spawnCloud(Vector2 position)
    {
        oldInstantiatedCloud = currentInstantiatedCloud;
        currentInstantiatedCloud = instantiatedCloud;
        rand = Random.Range(0f, csFormations);
        randomized = (int)rand;
        posRand = Random.Range(50f, 100f);
        if (randomized == 0 && gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {
            
            cloud.transform.position = position;
            instantiatedCloud = Instantiate(cloud);
        }
        else if (randomized == 1 &&  gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {
            
            cloud2.transform.position = position;
            instantiatedCloud = Instantiate(cloud);
        }
        else if (randomized == 2 && gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {
           
            cloud3.transform.position = position;
            instantiatedCloud = Instantiate(cloud3);
        }
        else if (randomized == 3 && gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {
          
            cloud4.transform.position = position;
            instantiatedCloud = Instantiate(cloud4);
        }
        else if (randomized == 4 && gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {
            
            cloud5.transform.position = position;
            instantiatedCloud = Instantiate(cloud5);
        }
        else if (randomized == 5 && gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {

            cloud6.transform.position = position;
            instantiatedCloud = Instantiate(cloud6);
        }
        else if (randomized == 6 && gameObject.GetComponent<BackgroundSystem>().counter < 60)
        {

            cloud7.transform.position = position;
            instantiatedCloud = Instantiate(cloud7);
            //vazia
        }
    
        else if (randomized == 7 && gameObject.GetComponent<BackgroundSystem>().counter > 90)
        {
            
            star.transform.position = position;
            instantiatedCloud = Instantiate(star);
        }
        else if (randomized == 8 && gameObject.GetComponent<BackgroundSystem>().counter > 90)
        {
            star2.transform.position = position;
            instantiatedCloud = Instantiate(star2);
        }
        else if (randomized == 9 && gameObject.GetComponent<BackgroundSystem>().counter > 90)
        {
            star2.transform.position = position;
            instantiatedCloud = Instantiate(star3);
        }
        else if (randomized == 10 && gameObject.GetComponent<BackgroundSystem>().counter > 90)
        {
            star2.transform.position = position;
            instantiatedCloud = Instantiate(star4);
            //vazia
        }

    }
    public void deleteCloud()
    {
        
        Destroy(oldInstantiatedCloud);
    }



}
