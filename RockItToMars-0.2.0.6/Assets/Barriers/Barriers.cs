﻿using UnityEngine;
using System.Collections;

public class Barriers : MonoBehaviour {
    public GameObject asteroid;
    public GameObject padre;
    public GameObject gaivota;
    public GameObject plane;
    private GameObject instantiatedBarrier;
    private GameObject currentInstantiatedBarrier;
    private GameObject oldinstantiatedBarrier;
    private bool padreSpawnou;
    public float bFormations;
    private float rand;
    public int randomized;
    private float posRand;
    
    void Start()
    {
        if (bFormations == 0)
            bFormations = 7f;
        padreSpawnou = false;
    }

    void Update()
    {

    }
    public void spawnBarrier(Vector2 position)
    {
        Vector3 positionV3 = position;
        oldinstantiatedBarrier = currentInstantiatedBarrier;
        currentInstantiatedBarrier = instantiatedBarrier;
        rand = Random.Range(0f, bFormations);
        randomized = (int)rand;
        posRand = Random.Range(-2.5f, 2.5f);
        if (randomized == 0 && gameObject.GetComponent<BackgroundSystem>().counter < 50)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;
            gaivota.transform.position = positionV3;
            instantiatedBarrier = Instantiate(gaivota);

        }
        else if (randomized == 1 && gameObject.GetComponent<BackgroundSystem>().counter > 80)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;
            asteroid.transform.position = positionV3;
            instantiatedBarrier = Instantiate(asteroid);
        }
        else if (randomized == 2 && gameObject.GetComponent<BackgroundSystem>().counter >50 && padreSpawnou == false)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;
            padre.transform.position = positionV3;
            instantiatedBarrier = Instantiate(padre);
            padreSpawnou = true;    
        }
        else if (randomized == 3 && gameObject.GetComponent<BackgroundSystem>().counter > 50 && padreSpawnou == false)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;
            padre.transform.position = positionV3;
            instantiatedBarrier = Instantiate(padre);
            padreSpawnou = true;
        }
        else if (randomized == 4 && gameObject.GetComponent<BackgroundSystem>().counter < 50)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;
            gaivota.transform.position = positionV3;
            instantiatedBarrier = Instantiate(gaivota);

        }
        else if (randomized == 5 && gameObject.GetComponent<BackgroundSystem>().counter > 80)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;

            asteroid.transform.position = positionV3;
            instantiatedBarrier = Instantiate(asteroid);
        }
        else if (randomized == 6 && gameObject.GetComponent<BackgroundSystem>().counter < 70)
        {
            if (posRand <= -1.9f)
                positionV3.x = posRand + 1.8f;
            else
                positionV3.x = posRand - 1.8f;
            positionV3.z = -3f;

            plane.transform.position = positionV3;
            instantiatedBarrier = Instantiate(plane);
        }
    }
    public void deleteBarrier()
    {
        Destroy(oldinstantiatedBarrier);
    }
}
