﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PlayMusic : MonoBehaviour {

    public AudioSource music;
        


    public AudioClip musicClip;



    public AudioMixerSnapshot volumeDown;           
    public AudioMixerSnapshot volumeUp;            

    // Use this for initialization
    void Awake ()
    {
        music.clip = musicClip;
        
     
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void musicPlay()
    {
        
        music.Play();
    }
    
    
    public void musicStop()
    {
        music.Stop();
    }


}
