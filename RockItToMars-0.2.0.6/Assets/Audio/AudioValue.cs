﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioValue : MonoBehaviour {


    public Slider sliderMusicPause;
    public Slider sliderMusic;
    public Slider sliderFxPause;
    public Slider sliderFx;


    public float valueMusic;
    public float valueMusicP;
    public float valueFx;
    public float valueFxP;






	// Use this for initialization
	void Start ()
    {
        sliderMusic.value = PlayerPrefs.GetFloat("musicVolume", 0f);
        sliderFx.value = PlayerPrefs.GetFloat("sfxVolume", 0f);
        sliderMusicPause.value = PlayerPrefs.GetFloat("musicVolume", 0f);
        sliderFxPause.value = PlayerPrefs.GetFloat("sfxVolume", 0f);


    }
	
	// Update is called once per frame
	void Update ()
    {
        
       

    }

    void Awake()
    {
       
    }



    public void getSliderValueOpt()
    {
        valueMusic = sliderMusic.value;
        valueFx = sliderFx.value;
        PlayerPrefs.SetFloat("musicVolume", valueMusic);
        PlayerPrefs.SetFloat("sfxVolume", valueFx);


    }

    public void getSliderValuePause()
    {
        
        valueMusicP = sliderMusicPause.value;
        valueFxP = sliderFxPause.value;
        PlayerPrefs.SetFloat("musicVolume", valueMusicP);
        PlayerPrefs.SetFloat("sfxVolume", valueFxP);

    }


    public void syncValuesOptPause()
    {
        sliderMusicPause.value = valueMusic;
        sliderFxPause.value = valueFx;

        


    }

    public void syncValuesPauseOpt()
    {
        sliderMusic.value = valueMusicP;
        sliderFx.value = valueFxP;
        
        
    }


   
    
}
