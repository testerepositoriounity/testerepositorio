﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PlaySfx : MonoBehaviour {

    public AudioSource liftOff;
    public AudioSource liftOffBtn;
    public AudioSource whileFly;
    public AudioSource getFuel;
    public AudioSource explosion;

    public AudioClip liftOffClip;
    public AudioClip liftOffBtnClip;
    public AudioClip whileFlyClip;
    public AudioClip getFuelClip;
    public AudioClip explosionClip;
 


    public AudioMixerSnapshot volumeDown;           
    public AudioMixerSnapshot volumeUp;            

    // Use this for initialization
    void Awake ()
    {
        liftOff.clip = liftOffClip;
        liftOffBtn.clip = liftOffBtnClip;
        whileFly.clip = whileFlyClip;
        getFuel.clip = getFuelClip;
        explosion.clip = explosionClip;
       

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void playLiftOff()
    {
        liftOff.Play();
    }
    public void playLiftOffBtn()
    {
        liftOffBtn.Play();
    }
    public void playWhileFly()
    {
        whileFly.Play();
    }
    public void playGetFuel()
    {
        getFuel.Play();
    }
    public void playExplosion()
    {
        explosion.Play();
    }


    public void stopLiftOff()
    {
        liftOff.Stop();
    }
    public void stopLiftOffBtn()
    {
        liftOffBtn.Stop();
    }
    public void stopWhileFly()
    {
        whileFly.Stop();
    }
    public void stopGetFuel()
    {
        getFuel.Stop();
    }
    public void stopExplosion()
    {
        explosion.Stop();
    }


}
