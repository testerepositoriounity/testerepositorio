﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetAudioLevels : MonoBehaviour {

	public AudioMixer mainMixer;					//Used to hold a reference to the AudioMixer mainMixer
    public Slider audioSlider;


    void Start()
    {
        audioSlider = gameObject.GetComponent<Slider>();
    }
	//Call this function and pass in the float parameter musicLvl to set the volume of the AudioMixerGroup Music in mainMixer
	public void SetMusicLevel(float musicLvl)
	{
		
        if (audioSlider.value == -40)
        {
            musicLvl = -80;
            mainMixer.SetFloat("musicVol", musicLvl);
        }
        else
        {
            mainMixer.SetFloat("musicVol", musicLvl);
        }
    }

	//Call this function and pass in the float parameter sfxLevel to set the volume of the AudioMixerGroup SoundFx in mainMixer
	public void SetSfxLevel(float sfxLevel)
	{
		
        if (audioSlider.value == -40)
        {
            sfxLevel = -80;
            mainMixer.SetFloat("sfxVol", sfxLevel);
        }
        else
        {
            mainMixer.SetFloat("sfxVol", sfxLevel);
        }
    }

    



    

}
